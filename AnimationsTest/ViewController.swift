//
//  ViewController.swift
//  AnimationsTest
//
//  Created by Юрий on 2/26/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    private var circlesCount = 0 {
        didSet {
            if circlesCount == 0 {
                createCircles()
            }
        }
    }

    fileprivate func createCircles() {
        var delay = 0.0
        circlesCount = 50
        
        for _ in 0..<circlesCount {
            let size = Double.random(in: 20...150)
            let x = Double.random(in: 0...Double(view.bounds.size.width) - size)
            let y = Double.random(in: 0...Double(view.bounds.size.height) - size)
            let x2 = x + size/2
            let y2 = y + size/2
            
            let startRect = CGRect(x: x2, y: y2, width: 0, height: 0)
            let endRect = CGRect(x: x, y: y, width: size, height: size)
            
            let circle = CircleView(frame: startRect)

            delay += 0.05
            circle.createAnmiation(frame: endRect, delay: delay)
            
            self.view.addSubview(circle)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        createCircles()
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapCircle(_:))))
        
        
    }
    
    @objc func tapCircle(_ sender: UITapGestureRecognizer) {
        let point = sender.location(in: view)
        let x1 = point.x
        let y1 = point.y
        
        
        for item in (self.view.subviews).reversed() {
            let x2 = item.center.x
            let y2 = item.center.y
            let d1 = sqrt(pow(x2 - x1, 2) + pow(y2 - y1, 2))
            
            if d1 < (item.frame.height/2), let items = item as? CircleView {
                items.tapCircleAndMakeAnimation()
                circlesCount -= 1
                break
            }
        }
    }
}

