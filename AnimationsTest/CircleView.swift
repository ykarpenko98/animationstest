//
//  CircleView.swift
//  AnimationsTest
//
//  Created by Юрий on 2/26/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit
class CircleView: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.backgroundColor = UIColor.red
        self.layer.cornerRadius = self.frame.width / 2
        self.layer.borderColor = UIColor.white.cgColor
        self.layer.borderWidth = 1.0
    }
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func tapCircleAndMakeAnimation() {
        UIView.animate(withDuration: 1.5, delay: 0, usingSpringWithDamping: 1.1, initialSpringVelocity: 1, options: .curveLinear , animations: {
            self.frame = self.frame(forAlignmentRect: CGRect(x: self.center.x, y: self.center.y, width: 1, height: 1))
            self.layer.cornerRadius = 0.5
            self.alpha = 0
        }) { (_) in
            self.removeFromSuperview()
        }
    }
    
    func createAnmiation(frame: CGRect, delay: Double) {
        UIView.animate(withDuration: 1.5, delay: delay, usingSpringWithDamping: 0.7, initialSpringVelocity: 0.1, options: .allowUserInteraction , animations: {
            self.frame = self.frame(forAlignmentRect: frame)
            self.layer.cornerRadius = self.frame.width / 2
        }){ (_) in
        }
    }
}
